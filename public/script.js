function onLoadScript() {
    let vote= localStorage.getItem("voting");
    if(vote) {
        if(vote==="bjp") {
            document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: #ea6709'>BJP&nbsp&nbsp(બીજેપી)</span>";
        } else if (vote === "congress") {
            document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: green'>Congress&nbsp&nbsp(કોંગ્રેસ)</span>";
        } else if (vote === "aap") {
            document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: dodgerblue'>AAP&nbsp&nbsp(આપ)</span>";
        } else if (vote === "other") {
            document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: gray'>Other&nbsp&nbsp(અન્ય)</span>";
        } else if (vote === "nota") {
            document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: red'>NOTA&nbsp&nbsp(કોઈ પણ નહિ)</span>";
        }

        document.getElementById("voteButton").disabled = true;

        let url = "https://voting-2022.herokuapp.com/getCount"
        //let url = "http://localhost:3000/getCount";
        let xr = new XMLHttpRequest();

        xr.open("POST", url, true);
        xr.setRequestHeader("Content-Type", "application/json");
        xr.onreadystatechange = function () {
            if (xr.readyState === 4 && xr.status === 200) {
                const json = (xr.responseText);
                printResponse(json);
            }
        }
        xr.send();
    }

}

function checkIfVoted() {
    let vote= localStorage.getItem("voting");
    if(vote==="bjp") {
        document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: #ea6709'>BJP&nbsp&nbsp(બીજેપી)</span>";
    } else if (vote === "congress") {
        document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: green'>Congress&nbsp&nbsp(કોંગ્રેસ)</span>";
    } else if (vote === "aap") {
        document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: dodgerblue'>AAP&nbsp&nbsp(આપ)</span>";
    } else if (vote === "other") {
        document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: gray'>Other&nbsp&nbsp(અન્ય)</span>";
    } else if (vote === "nota") {
        document.getElementById("alreadyVoted").innerHTML = "તમે મત આપ્યો છે: &nbsp <span class='yourVote' style='color: red'>NOTA&nbsp&nbsp(કોઈ પણ નહિ)</span>";
    }
    document.getElementById("voteButton").disabled = true;

}

function voteCount() {
    let long = document.getElementById("long").value;
    let lat = document.getElementById("lat").value;

    let url = "https://voting-2022.herokuapp.com"
    //let url = "http://localhost:3000";
    let xr = new XMLHttpRequest();
    let flag = true;
    let x = document.getElementsByName("vote");

    for(let i=0; i<x.length; i++) {
        if(x[i].checked) {
            flag = false;
            const data = {
                name: x[i].value,
                long: long,
                lat: lat
            };
            localStorage.setItem("voting", x[i].value);
            url = url+"/voteCount";

            xr.open("POST", url, true);
            xr.setRequestHeader("Content-Type", "application/json");
            xr.onreadystatechange = function () {
                if (xr.readyState === 4 && xr.status === 200) {
                    const json = (xr.responseText);
                    checkIfVoted();
                    printResponse(json);
                }
            }
            xr.send(JSON.stringify(data));
        }
    }
    if(flag) {
        alert('Please select at least one party');
        return false;
    }
    return true;
}

function fillArrow(x) {
    let party = ["bjp", "congress", "aap", "other", "nota"];
    for(let i=0;i<party.length;i++) {
        let id = 'rButton'+party[i];
        if(party[i]===x) {
            document.getElementById(id).innerHTML = "✔";
        } else {
            document.getElementById(id).innerHTML = "&nbsp&nbsp";
        }
    }
}

function printResponse(json) {
    json = JSON.parse(json)
    let totalVote = json[0] + json[1] + json[2] + json[3] +json[4]
    let bjpPer = (json[1]/totalVote)*100;
    let congressPer = (json[2]/totalVote)*100;
    let aapPer = (json[0]/totalVote)*100;
    let otherPer = (json[4]/totalVote)*100;
    let notaPer = (json[3]/totalVote)*100;

    let str=`
        <span class="liveResult">રિઝલ્ટ</span>
        <br>
        <div class="resultLabel"><span>BJP</span></div>
        <div class="container123">
          <div class="skills" style="color:black;background-color: #ea6709; width: ${bjpPer}%">${json[1]}&nbsp(${bjpPer.toFixed(2)}%)</div>
        </div>
        <br><br>
        
        <div class="resultLabel"><span>AAP</span></div>
        <div class="container123">
          <div class="skills" style="color:black;background-color: dodgerblue; width: ${aapPer}%">${json[0]}&nbsp(${aapPer.toFixed(2)}%)</div>
        </div>
        <br><br>
        
        <div class="resultLabel"><span>Congress</span></div>
        <div class="container123">
          <div class="skills" style="color:black;background-color: green; width: ${congressPer}%">${json[2]}&nbsp(${congressPer.toFixed(2)}%)</div>
        </div>
        <br><br>
        
        <div class="resultLabel"><span>Other</span></div>
        <div class="container123">
          <div class="skills" style="color:black;background-color: gray; width: ${otherPer}%">${json[4]}&nbsp(${otherPer.toFixed(2)}%)</div>
        </div>
        <br><br>
        
        <div class="resultLabel"><span>NOTA</span></div>
        <div class="container123">
          <div class="skills" style="color:black;background-color: red; width: ${notaPer}%">${json[3]}&nbsp(${notaPer.toFixed(2)}%)</div>
        </div>
    `;

    document.getElementById("result").innerHTML = str;
}
