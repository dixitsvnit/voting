const bodyParser = require('body-parser');
const fs = require('fs');
const session = require('express-session');
const express = require('express');
const app = express();

const cors = require('cors');
const port = process.env.PORT || 3000;

const db_con = require('./public/sqlConnection');


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use('/',express.static('public'));
app.use(session({
    secret: 'secret insta',
    resave:true,
    saveUninitialized:true
}));



app.post('/voteCount',function(req,res){

    const name = req.body.name;
    const long = req.body.long;
    const lat = req.body.lat;
    let party = ["bjp", "congress", "aap", "other", "nota"];
    let arr = [];
    try {
        fs.appendFileSync('file/location.txt', long+" "+lat+" "+name+"\n");

        queryPromise("Update vote_count set coun = coun+1 where party=?", [name]).then(result => {

        });

        queryPromise("select party, coun from  vote_count order by party", []).then(result => {
            console.log(result);
            arr.push(result[0].coun);
            arr.push(result[1].coun);
            arr.push(result[2].coun);
            arr.push(result[3].coun);
            arr.push(result[4].coun);
            res.send(arr)

        });

    } catch (err) {
        console.error(err);
    }    

});

function queryPromise(str, params) {
    return new Promise((resolve, reject) => {
        db_con.query(str, params, (err, result, fields) => {
            if (err) reject(err);
            resolve(result);
        })
    })
}

app.post('/getCount',function(req,res){
    let party = ["bjp", "congress", "aap", "other", "nota"];
    let arr = [];
    try {

        queryPromise("select party, coun from  vote_count order by party", []).then(result => {
            console.log(result);
            arr.push(result[0].coun);
            arr.push(result[1].coun);
            arr.push(result[2].coun);
            arr.push(result[3].coun);
            arr.push(result[4].coun);
            res.send(arr)
        });
    } catch (err) {
        console.error(err);
    }

});

app.get('/getlocation123xyzlocationabc',(req,res) => {
    let arr = [];
    let data = fs.readFileSync('file/location.txt', 'UTF-8');
    let lines = data.split(/\r?\n/);
    lines.forEach((line) => {
        arr.push(line);
    });
    return res.send(arr);
});

app.get('/keepAlive',(req,res) => {
    return res.send("I am alive");
});

app.listen(port, ()=>console.log(`app listening at ${port}`));

